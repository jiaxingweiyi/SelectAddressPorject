//
//  HotCityHeaderView.h
//  NNHealthMall
//
//  Created by 蓓蕾 on 2021/9/2.
//  Copyright © 2021 YY. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HotCityConfig : NSObject

@property (nonatomic, assign) CGFloat leftMargin;

@property (nonatomic, assign) CGFloat rightMargin;

@property (nonatomic, assign) CGFloat topMargin;

@property (nonatomic, assign) CGFloat bottomMargin;

@property (nonatomic, assign) CGFloat itemHorizontalMargin;

@property (nonatomic, assign) CGFloat itemVerticalMargin;

@property (nonatomic, assign) NSInteger itemCount;

@property (nonatomic, assign) CGFloat itemWidth;

@property (nonatomic, assign) CGFloat itemHeight;

@end

NS_ASSUME_NONNULL_BEGIN

@interface HotCityHeaderView : UIView

-(id)initWithConfig:(HotCityConfig *)config;

@property (nonatomic, strong) NSArray *cityArr;

@property (nonatomic, strong) NSString *titleString;

@property (nonatomic, assign) BOOL showHeaderView;

@property (nonatomic, assign) CGFloat viewHeight;

@property (nonatomic, assign) BOOL autoHeight;

@property (nonatomic, copy) void (^CityViewBlock)(UIButton *sender, NSString *title);

//-(CGFloat)getViewHeight:(NSArray *)array;

@end

NS_ASSUME_NONNULL_END
