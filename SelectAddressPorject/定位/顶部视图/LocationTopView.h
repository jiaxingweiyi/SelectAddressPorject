//
//  LocationTopView.h
//  NNHealthMall
//
//  Created by 蓓蕾 on 2021/9/2.
//  Copyright © 2021 YY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocationTopView : UIView

@property (nonatomic, strong) NSString *locationString;

@property (nonatomic, copy) void (^SelectAreaBlock)(BOOL show);

@property (nonatomic, assign) BOOL show;

@end

NS_ASSUME_NONNULL_END
