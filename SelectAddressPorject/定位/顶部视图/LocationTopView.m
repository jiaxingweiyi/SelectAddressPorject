//
//  LocationTopView.m
//  NNHealthMall
//
//  Created by 蓓蕾 on 2021/9/2.
//  Copyright © 2021 YY. All rights reserved.
//

#import "LocationTopView.h"

@interface LocationTopView ()

@property (nonatomic, strong) UILabel *locationView;
@property (nonatomic, strong) UIButton *moreView;

@end

@implementation LocationTopView

-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        _moreView = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_moreView];
        [_moreView setImage:IMAGE(@"选择区县上") forState:UIControlStateNormal];
        [_moreView setImage:IMAGE(@"选择区县下") forState:UIControlStateSelected];
        [_moreView addTarget:self action:@selector(moreMethod:) forControlEvents:UIControlEventTouchUpInside];
        [_moreView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.mas_centerY).mas_offset(0);
            make.right.mas_equalTo(self.mas_right).mas_offset(-20);
            make.size.mas_equalTo(IMAGE(@"选择区县下").size);
        }];
        _locationView = [[UILabel alloc] init];
        _locationView.textColor = rgbColor(0x333333);
        _locationView.text = @"当前城市: 郑州市";
        _locationView.font = [UIFont boldSystemFontOfSize:20];
        [self addSubview:_locationView];
        _locationView.adjustsFontSizeToFitWidth = YES;
        [_locationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).mas_offset(20);
            make.right.mas_lessThanOrEqualTo(_moreView.mas_left).mas_offset(-5);
            make.centerY.mas_equalTo(self.mas_centerY).mas_offset(0);
        }];
        
        
//        _show = YES;
//        _moreView.selected = YES;
    }
    return self;
}

-(void)moreMethod:(UIButton *)sender
{
    sender.selected = !sender.selected;
    _show = sender.selected;
    if (self.SelectAreaBlock) {
        self.SelectAreaBlock(sender.selected);
    }
}

-(void)setLocationString:(NSString *)locationString
{
    _locationString = locationString;
    _locationView.text = locationString;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
