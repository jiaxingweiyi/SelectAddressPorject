//
//  LocationTitlesHeaderView.m
//  NNHealthMall
//
//  Created by 蓓蕾 on 2021/9/2.
//  Copyright © 2021 YY. All rights reserved.
//

#import "LocationTitlesHeaderView.h"

@interface LocationTitlesHeaderView ()

@property (nonatomic, strong) UILabel *titleView;

@end

@implementation LocationTitlesHeaderView

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        _titleView = [[UILabel alloc] init];
        _titleView.textColor = rgbColor(0x333333);
        _titleView.font = [UIFont boldSystemFontOfSize:15];
        [self.contentView addSubview:_titleView];
        [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).mas_offset(20);
            make.centerY.mas_equalTo(self.contentView.mas_centerY).mas_offset(0);
        }];
    }
    return self;
}

-(void)setTitleString:(NSString *)titleString
{
    _titleString = titleString;
    _titleView.text = titleString;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
