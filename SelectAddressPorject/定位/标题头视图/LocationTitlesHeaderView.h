//
//  LocationTitlesHeaderView.h
//  NNHealthMall
//
//  Created by 蓓蕾 on 2021/9/2.
//  Copyright © 2021 YY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocationTitlesHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) NSString *titleString;

@end

NS_ASSUME_NONNULL_END
